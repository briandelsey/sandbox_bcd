﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml.Serialization;
using Amazon.SQS;
using Amazon.SQS.Model;

namespace ScrapeService
{
    /// <summary>
    /// Implementation of <see cref="IInputPort"/> that accepts scrape requests from an AWS queue.
    /// </summary>
    public class SqsInputPort : IInputPort
    {
        /// <summary>
        /// CTor
        /// </summary>
        /// <param name="client">The AWS SQS client</param>
        /// <param name="queueName">The name of the queue</param>
        public SqsInputPort(AmazonSQSClient client, string queueName)
        {
            ThreadPool.QueueUserWorkItem(RequestMonitor, new Tuple<AmazonSQSClient, string>(client, queueName));            
        }

        /// <summary>
        /// See <see cref="IInputPort.ScrapeRequestEvent"/>.
        /// </summary>
        public event EventHandler<ScrapeRequest> ScrapeRequestEvent;

        /// <summary>
        /// Wait on requests from our queue... when received, fire <see cref="ScrapeRequestEvent"/>.
        /// </summary>
        /// <param name="state"></param>
        private void RequestMonitor(object state)
        {
            AmazonSQSClient client = (state as Tuple<AmazonSQSClient, string>).Item1;
            string queueName = (state as Tuple<AmazonSQSClient, string>).Item2;

            var url = client.ListQueues(queueName).QueueUrls.FirstOrDefault();

            var qRequest = new ReceiveMessageRequest { MaxNumberOfMessages = 1, QueueUrl = url };
            var serializer = new XmlSerializer(typeof(ScrapeRequest));

            while (true)
            {
                var response = client.ReceiveMessage(qRequest);
                var message = response.Messages != null && response.Messages.Count > 0 ? response.Messages[0] : null;

                if (message != null)
                {
                    client.DeleteMessage(new DeleteMessageRequest(url, message.ReceiptHandle));

                    using (var sr = new StringReader(message.Body))
                    {
                        ScrapeRequest request = serializer.Deserialize(sr) as ScrapeRequest;
                        ScrapeRequestEvent?.Invoke(this, request);
                    }
                }
            }
        }
    }
}
