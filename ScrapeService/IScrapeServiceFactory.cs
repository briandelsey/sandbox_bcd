﻿namespace ScrapeService
{
    /// <summary>
    /// Simple <see cref="IScrapeService"/> factory interface.
    /// </summary>
    public interface IScrapeServiceFactory
    {
        /// <summary>
        /// Returns a reference to a stateless service that implements the <see cref="IScrapeService"/> interface for the specified site identifier.
        /// </summary>
        /// <param name="siteIdentifier">The identifier of the site to be supported by the scraper.</param>
        /// <returns></returns>
        IScrapeService GetService(string siteIdentifier);
    }
}
