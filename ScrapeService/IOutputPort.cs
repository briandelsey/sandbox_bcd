﻿namespace ScrapeService
{
    public interface IOutputPort
    {
        /// <summary>
        /// Publish the results of a scrape.
        /// </summary>
        /// <param name="results">The scrape results. See <see cref="ScrapeResult"/></param>
        void PublishResults(ScrapeResult results);
    }
}
