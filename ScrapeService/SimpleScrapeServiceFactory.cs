﻿using System;
using System.Collections.Concurrent;

namespace ScrapeService
{
    /// <summary>
    /// This implementation of <see cref="IScrapeServiceFactory"/> serves up local instances of <see cref="IScrapeService"/> objects. Other
    /// factories may serve up references to remote services or internal threads etc.
    /// </summary>
    public class SimpleScrapeServiceFactory : IScrapeServiceFactory
    {
        /// <summary>
        /// CTor
        /// </summary>
        public SimpleScrapeServiceFactory()
        {
            // load up our dependencies... maybe from config our elsewhere... herein just hardcoded for demo purposes...
            var a = new ScrapeServiceA();
            var b = new ScrapeServiceB();

            _servicesCahce.TryAdd(a.SiteIdentifier, a);
            _servicesCahce.TryAdd(b.SiteIdentifier, b);
        }

        /// <summary>
        /// See <see cref="IScrapeServiceFactory.GetService"/>
        /// </summary>
        public IScrapeService GetService(string siteIdentifier)
        {
            if (_servicesCahce.ContainsKey(siteIdentifier))
            {
                return _servicesCahce[siteIdentifier];
            }

            throw new InvalidOperationException("There is no known scraper service for site identifier " + siteIdentifier);
        }

        private readonly ConcurrentDictionary<string, IScrapeService> _servicesCahce = new ConcurrentDictionary<string, IScrapeService>();
    }
}
