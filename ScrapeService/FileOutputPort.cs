﻿using System.IO;

namespace ScrapeService
{
    /// <summary>
    /// Implementation of <see cref="IOutputPort"/> that simply dumps to the specified file.
    /// </summary>
    public class FileOutputPort : IOutputPort
    {
        /// <summary>
        /// CTor
        /// </summary>
        /// <param name="filePath">Output file.</param>
        public FileOutputPort(string filePath)
        {
            _filePath = filePath;
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
        }

        /// <summary>
        /// See <see cref="IOutputPort.PublishResults"/>.
        /// </summary>
        public void PublishResults(ScrapeResult results)
        {
            File.AppendAllText(_filePath, $"[{results.ScrapeCriteria}]: {results.Result}");
        }

        private readonly string _filePath;
    }
}
