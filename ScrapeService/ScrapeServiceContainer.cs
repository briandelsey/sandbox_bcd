﻿using System.Threading;

namespace ScrapeService
{
    /// <summary>
    /// Instantiate this for any scrape service.... This container is what actually exposes the serviceFactory to the world.
    /// </summary>
    public class ScrapeServiceContainer
    {
        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="serviceFactory">Reference to the scrape service factory that can serve up instances of scrape services for us.</param>
        /// <param name="inputPort">Reference to the input port for this capability providing physical implementation of inbound requests.</param>
        /// <param name="outputPort">Reference to the output port for this capability providing physical implementation of outbound data.</param>
        /// <param name="runAsynchronously">Whether or not scrape requests are processed asynchronously.</param>
        public ScrapeServiceContainer(IScrapeServiceFactory serviceFactory, IInputPort inputPort, IOutputPort outputPort, bool runAsynchronously)
        {
            _serviceFactory = serviceFactory;
            _inputPort = inputPort;
            _outputPort = outputPort;
            _runAsynchronously = runAsynchronously;

            _inputPort.ScrapeRequestEvent += OnScrapeRequestEvent;
        }

        /// <summary>
        /// Request from the input port to scrape a site. Marshal to the appropriate scraper service.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="request"></param>
        private void OnScrapeRequestEvent(object sender, ScrapeRequest request)
        {
            if (_runAsynchronously)
            {
                ThreadPool.QueueUserWorkItem(ProcessRequest, request);
            }
            else
            {
                ProcessRequest(request);
            }
        }

        private void ProcessRequest(object state)
        {
            ScrapeRequest request = state as ScrapeRequest;

            IScrapeService scraper = _serviceFactory.GetService(request.SiteId);
            _outputPort.PublishResults(scraper.Scrape(request));
        }

        private readonly IScrapeServiceFactory _serviceFactory;
        private readonly IInputPort _inputPort;
        private readonly IOutputPort _outputPort;
        private readonly bool _runAsynchronously;
    }
}
