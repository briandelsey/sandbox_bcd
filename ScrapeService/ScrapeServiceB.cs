﻿using System;
using System.Threading;

namespace ScrapeService
{
    /// <summary>
    /// Implementation of <see cref="IScrapeService"/> for site B
    /// </summary>
    public class ScrapeServiceB : IScrapeService
    {
        #region IScrapeService Implementation

        /// <summary>
        /// See <see cref="IScrapeService.SiteIdentifier"/>
        /// </summary>
        public string SiteIdentifier => "B";

        /// <summary>
        /// See <see cref="IScrapeService.Scrape"/>
        /// </summary>
        public ScrapeResult Scrape(ScrapeRequest request)
        {
            Thread.Sleep(_rand.Next(50, 2000));
            return new ScrapeResult(request) {Result = "Scraper B results..."};
        }

        #endregion

        private readonly Random _rand = new Random("B".GetHashCode());
    }
}
