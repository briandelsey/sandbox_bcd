﻿namespace ScrapeService
{
    /// <summary>
    /// Interface to the simple scrape service.
    /// </summary>
    public interface IScrapeService
    {
        /// <summary>
        /// Returns the identifier of the site that this scrape service is associated with.
        /// </summary>
        string SiteIdentifier { get; }

        /// <summary>
        /// Scrape your site and return the results in a string.
        /// </summary>
        /// <param name="request">The criteria of the request. See <see cref="ScrapeRequest"/></param>
        /// <returns>The scrape results. See <see cref="ScrapeResult"/></returns>
        ScrapeResult Scrape(ScrapeRequest request);
    }
}
