﻿namespace ScrapeService
{
    /// <summary>
    /// Encapsulates a request to scrape a specific site for specific data.
    /// </summary>
    public class ScrapeRequest
    {
        /// <summary>
        /// For serialization...
        /// </summary>
        public ScrapeRequest()
        {
        }

        /// <summary>
        /// CTor
        /// </summary>
        /// <param name="requestSiteId"></param>
        /// <param name="requestScrapeCriteria"></param>
        public ScrapeRequest(string requestSiteId, string requestScrapeCriteria)
        {
            SiteId = requestSiteId;
            ScrapeCriteria = requestScrapeCriteria;
        }

        /// <summary>
        /// The ID of the site we need to scrape. (e.g. SkyScanner)
        /// </summary>
        public string SiteId;

        /// <summary>
        /// What are we scraping? This would obviously be an Itinerary object or something more concrete.
        /// </summary>
        public string ScrapeCriteria;
    }
}
