﻿namespace ScrapeService
{
    /// <summary>
    /// Encapsulates a result of a scrape.
    /// </summary>
    public class ScrapeResult : ScrapeRequest
    {
        public ScrapeResult(ScrapeRequest request)
            : base(request.SiteId, request.ScrapeCriteria)
        {            
        }

        /// <summary>
        /// The scrape results as a string (this could actually be JSON or a concrete class or XML etc...)
        /// </summary>
        public string Result;
    }
}
