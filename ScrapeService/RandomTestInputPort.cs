﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Xml.Serialization;

namespace ScrapeService
{
    /// <summary>
    /// Implementation of <see cref="IInputPort"/> that randomly generates a number of scrape requests.
    /// </summary>
    public class RandomTestInputPort : IInputPort
    {
        /// <summary>
        /// CTor
        /// </summary>
        /// <param name="siteIds">A collection of site IDs to be scraped</param>
        /// <param name="maxNumberRequests">Maximum number of requests to generate</param>
        public RandomTestInputPort(IEnumerable<string> siteIds, int maxNumberRequests)
        {
            ThreadPool.QueueUserWorkItem(RequestGenerator, new Tuple<IEnumerable<string>, int>(siteIds, maxNumberRequests));            
        }

        /// <summary>
        /// See <see cref="IInputPort.ScrapeRequestEvent"/>.
        /// </summary>
        public event EventHandler<ScrapeRequest> ScrapeRequestEvent;

        /// <summary>
        /// Wait a bit and then randomly generate some scrape requests.
        /// </summary>
        /// <param name="state"></param>
        private void RequestGenerator(object state)
        {
            IEnumerable<string> siteIds = (state as Tuple<IEnumerable<string>, int>).Item1;
            int maxNumRequests = (state as Tuple<IEnumerable<string>, int>).Item2;

            var sites = new Dictionary<int, string>();

            int siteCount = 0;
            foreach (string id in siteIds)
            {
                sites.Add(++siteCount, id);
            }

            var serializer = new XmlSerializer(typeof(ScrapeRequest));

            Random rand = new Random(DateTime.Now.Millisecond);

            Thread.Sleep(rand.Next(500, 5000));

            int numRequests = rand.Next(1, maxNumRequests);
            for (int i = 1; i <= numRequests; i++)
            {
                int key = rand.Next(1, sites.Count + 1);
                ScrapeRequestEvent?.Invoke(this, new ScrapeRequest(sites[key], i.ToString()));
                Thread.Sleep(rand.Next(5, 50));
            }
        }
    }
}
