﻿using System;

namespace ScrapeService
{
    /// <summary>
    /// Implementation of <see cref="IOutputPort"/> that simply dumps to the console.
    /// </summary>
    public class ConsoleOutputPort : IOutputPort
    {
        /// <summary>
        /// See <see cref="IOutputPort.PublishResults"/>.
        /// </summary>
        public void PublishResults(ScrapeResult results)
        {
            Console.WriteLine("[{0}]: {1}", results.ScrapeCriteria, results.Result);
        }
    }
}
