﻿using System;

namespace ScrapeService
{
    /// <summary>
    /// Interface for an input port for the scraper service capability providing physical implementation of inbound requests.
    /// </summary>
    public interface IInputPort
    {
        /// <summary>
        /// Event fired whenever a scrape request is received from an external source.
        /// </summary>
        event EventHandler<ScrapeRequest> ScrapeRequestEvent;
    }
}
