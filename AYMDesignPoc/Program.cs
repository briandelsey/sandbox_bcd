﻿using System;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Amazon;
using Amazon.Runtime;
using Amazon.SQS;
using ScrapeService;

namespace AYMDesignPoc
{
    class Program
    {
        static void Main(string[] args)
        {
            RunSystemArchitecture1(runAsynchronously: false);
            RunSystemArchitecture1(runAsynchronously: true);
            RunSystemArchitecture2();

            Console.WriteLine("Hit enter to terminate when you feel the demo is complete... (i.e. file written and console logs complete)");
            Console.ReadLine();
        }

        /// <summary>
        /// In this architecture, scrape requests are received from an SQS queue and results are dumped to the console.
        /// </summary>
        private static void RunSystemArchitecture1(bool runAsynchronously)
        {
            const string queueName = "bcd_q1";
            var credentials = new BasicAWSCredentials("AKIAIFWWVVNIKGVTQVQQ", "4ZlNAVXIC/zpggYxATBdGr4oixtQ12Z1enbynsbd");
            var sqsClient = new AmazonSQSClient(credentials, RegionEndpoint.USEast1);

            ScrapeServiceContainer ssc = new ScrapeServiceContainer(
                new SimpleScrapeServiceFactory(),
                new SqsInputPort(sqsClient, queueName),
                new ConsoleOutputPort(),
                runAsynchronously
                );

            // Push a few requests to the input port!
            var url = sqsClient.ListQueues(queueName).QueueUrls.FirstOrDefault();
            int requestId = 0;
            PostToQueue("A", sqsClient, url, ++requestId);
            PostToQueue("A", sqsClient, url, ++requestId);
            PostToQueue("B", sqsClient, url, ++requestId);
            PostToQueue("A", sqsClient, url, ++requestId);
            PostToQueue("B", sqsClient, url, ++requestId);
            PostToQueue("A", sqsClient, url, ++requestId);
            PostToQueue("A", sqsClient, url, ++requestId);

            // Input port will dequeue these from the queue and raise event to the scraper service container
            // Scraper service container will marshal the request to the desired scraper
            // Scraper service container will send the scrape results to the output port, which in this case simply dumps to console
        }

        /// <summary>
        /// In this architecture, scrape requests are received from a random generator and results are dumped to file.
        /// </summary>
        private static void RunSystemArchitecture2()
        {
            ScrapeServiceContainer ssc = new ScrapeServiceContainer(
                new SimpleScrapeServiceFactory(),
                new RandomTestInputPort(new [] {"A", "B"}, 100),
                new FileOutputPort("ScrapeResults.txt"),
                runAsynchronously: false
                );

            // Input port will randomly raise scrape requests events to the scraper service container
            // Scraper service container will marshal the request to the desired scraper
            // Scraper service container will send the scrape results to the output port, which in this case simply dumps to file
        }

        /// <summary>
        /// Post the the SQS queue.
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="sqsClient"></param>
        /// <param name="url"></param>
        /// <param name="requestId"></param>
        private static void PostToQueue(string siteId, AmazonSQSClient sqsClient, string url, int requestId)
        {
            var serializer = new XmlSerializer(typeof(ScrapeRequest));

            using (var sw = new StringWriter())
            {
                serializer.Serialize(sw, new ScrapeRequest(siteId, requestId.ToString()));
                sqsClient.SendMessage(url, sw.ToString());
            }
        }
    }
}
